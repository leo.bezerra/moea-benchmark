# moea-benchmark

Benchmark results from the experimental analysis conducted in the following paper:

> Leonardo C. T. Bezerra, Manuel López-Ibáñez, and Thomas Stützle. A large-scale experimental evaluation of high-performing multi- and many-objective evolutionary algorithms. Evolutionary Computation, 2018.

## Organization

* default-: results of MOEA runs using default parameter settings.
* tuned-: results of MOEA runs using tuned parameter settings.
* fronts.tar.xz: approximation fronts used in the assessment.
